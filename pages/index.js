import * as React from "react";

import Link from 'next/link'
import Layout from "../src/containers/Layout/Layout";
import PostsContainer from "../src/containers/PostsContainer/PostsContainer";

const Index = (props) => (
  <Layout title="Main Page">
    <PostsContainer />
  </Layout>
)

export default Index;