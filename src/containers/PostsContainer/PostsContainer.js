import React, {Component} from 'react';
import {Grid, LinearProgress} from "material-ui";
import Drawer from "../../components/PostsDrawer/PostsDrawer";
import Post from "../../components/Post/Post";
import InfiniteScroll from 'react-infinite-scroller';
import PostAPI from "../../middleware/postApi";
import DrawerItem from "../../components/DrawerItem/DrawerItem";


class Tab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      drawItems: []
    }
  }

  loadPosts = (page) => {
    PostAPI.getPosts(page).then(data => {
      let posts = [];
      let drawItems = [];
      data.map((post) => {
        let date = new Date(post.publish_date);

        posts.push(
          <Post
            key={post.uuid}
            tags={post.tags}
            text={post.text}
            isPublic={post.public}
            publishDate={date}
          />);

        drawItems.push(
          <DrawerItem
            key={post.uuid}
            tags={post.tags}
            isPublic={post.public}
            publishDate={date}
          />);

        return true
      });
      this.setState((prevState, props) => {
        return {
          posts: prevState.posts.concat(posts),
          drawItems: prevState.drawItems.concat(drawItems)
        }
      });
    })
  };

  render() {
    return (
      <Grid container>
        <Grid item xs={9}>
          <InfiniteScroll
            pageStart={0}
            loadMore={this.loadPosts.bind(this)}
            hasMore={true || false}
            useWindow={true}
            loader={this.state.posts.length !== 0 ? <LinearProgress mode="indeterminate"/> : <div/>}
          >
            {this.state.posts}
          </InfiniteScroll>
        </Grid>
        <Drawer>
          {this.state.drawItems}
        </Drawer>
      </Grid>
    )
  }
}


export default Tab
