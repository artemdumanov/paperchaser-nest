import React, {Component} from 'react';
import {AppBar, Toolbar, IconButton, Button, Typography} from 'material-ui';
import MenuIcon from 'material-ui-icons/Menu';

const Bar = () => (
  <div>
    <AppBar position="static">
      <Toolbar>
        <IconButton color="contrast" aria-label="Menu">
          <MenuIcon/>
        </IconButton>
        <Typography type="title" color="inherit">
          Title
        </Typography>
        <Button color="contrast">Login</Button>
      </Toolbar>
    </AppBar>
  </div>
);


export default Bar;