const API_URL = 'http://0.0.0.0:8000/api';

class PostAPI {
  static getPosts(page) {
    const params = {
      method: 'GET',
      redirect: 'follow',
      headers: {
        'Accept': 'application/json',
      }
    };
    return fetch(`${API_URL}/posts/?page=${page}`, params)
      .then(response => {
        return response.json()
      })
      .then(data => {
        return data.results
      });
  }
}

export default PostAPI;
