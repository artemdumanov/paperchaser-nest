import React from 'react'
import {Card, CardActions, CardContent, Chip, Grid} from "material-ui"
import dateFormat from 'dateformat'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

const styles = {
  header: {
    display: 'flex'
  },
  post: {
    display: 'flex',
    padding: 16
  }
}

const Post = ({tags, text, publishDate, isPublic, classes}) => (
  <Card>
    <Grid item xs={12} className={classes.post}>
      <div className={classes.header}>
        {tags.map((tag, index) => {
          return (
            <Chip
              key={index}
              label={tag}
            />
          )
        })}
      </div>
      <label>{dateFormat(publishDate, "dd.mm.yyyy HH:MM:ss")}</label>
    </Grid>
    <CardContent>
      {text}
    </CardContent>
    <CardActions>

    </CardActions>
  </Card>
);

Post.propTypes = {
  tags: PropTypes.array.isRequired,
  text: PropTypes.string.isRequired,
  publishDate: PropTypes.instanceOf(Date).isRequired
};

export default withStyles(styles)(Post);