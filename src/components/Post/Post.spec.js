import React from 'react';
import {shallow} from 'enzyme';
import Post from "./Post";

describe('test post', () => {
    it('renders with crashing with props', () => {
      expect(() => {
        shallow(<Post/>);
      }).toThrow()
    });

    it('renders with props', () => {
      shallow(<Post tags={['tag', 'tag1']} text={'test text'} publishDate={new Date()}/>)
    })
  }
);

