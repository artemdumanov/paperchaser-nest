import React from 'react';
import {Grid} from "material-ui";
import PropTypes from 'prop-types';

const PostsDrawer = ({children}) => {
  return <Grid item xs={3}>
    {children}
  </Grid>
};

PostsDrawer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PostsDrawer;