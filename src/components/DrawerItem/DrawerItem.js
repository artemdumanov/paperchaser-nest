import React from 'react';
import PropTypes from 'prop-types';
import dateFormat from 'dateformat'

const DrawerItem = ({tags, isPublic, publishDate}) => (
  <label>{dateFormat(publishDate, "dd.mm.yyyy HH:MM:ss")} {tags} {isPublic}</label>
);

DrawerItem.propTypes = {
  tags: PropTypes.array.isRequired,
  isPublic: PropTypes.bool.isRequired,
  publishDate: PropTypes.instanceOf(Date).isRequired
};

export default DrawerItem;
