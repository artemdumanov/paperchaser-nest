FROM node:8.7.0
RUN apt-get update

WORKDIR /app
ADD . /app

RUN npm install
RUN npm build

EXPOSE 3000

CMD npm start
